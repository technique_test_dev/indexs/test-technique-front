const userService = require('./services/user-service');
const hotelService = require('./services/hotel-service');
const priceService = require('./services/price-service');
const { distance } = require('./services/helper');
const { getPricesByHotelsAndFare } = require('./services/price-service');

function findHotelsNearby(lat, lng, radius) {
    // TODO implement me
    let output = []

    if (!lat || !lng || !radius)
        return output

    hotelService
    .getHotels()
    .forEach(hotel=>{

        let dist = distance(lat, lng, hotel.latitude, hotel.longitude)

        if(radius >= dist){
            output.push({
                ridCode: hotel.ridCode,
                countryCode: hotel.countryCode,
                localRating: hotel.localRating,
                address: hotel.address,
                commercialName: hotel.commercialName,
                distance: dist,
            })
        }
            
    })

	return output;
}

function findHotelNearbyWithBestOffer(lat, lng, radius, date) {

    let hotels = findHotelsNearby(lat, lng, radius)

    const prices = getPricesByHotelsAndFare(hotels, date)

    if(prices.length<1)
        return null

    const hotels_prices = hotels.map(hotel=>{
        const obj = prices.find(price=>price.ridCode == hotel.ridCode)
        return {...hotel, ...obj}
    })

    let best_hotels = hotels_prices.sort((a,b)=> a.offers.price - b.offers.price)

    best_hotels = best_hotels.filter(best=>best.offers.price ==  best_hotels[0].offers.price)
    
    if(best_hotels.length>1)
        return best_hotels.sort((a,b)=> a.distance - b.distance)[0]
    else
        return best_hotels[0]
    
}

module.exports = {
	findHotelsNearby: findHotelsNearby,
	findHotelNearbyWithBestOffer: findHotelNearbyWithBestOffer
}
