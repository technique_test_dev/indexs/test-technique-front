const PRICES = require('./data/prices.json').prices;

const getPrices = () => {
	return PRICES;
}

const getPricesByHotelsAndFare = (hotels, date) => {

	let output = []

	if(!hotels || hotels.length < 1 || !date)
		return output

	output = PRICES.filter( price => hotels.some(hotel => hotel.ridCode == price.ridCode ))

	output = output.map(out=> {
		out.offers = out.offers.filter(offer => offer.fare == "STANDARD" && offer.date == date)

		out.offers = out.offers.sort((a,b)=>a.price - b.price)[0]

		return out
	})

	return output
}

module.exports = {
	getPrices: getPrices,
	getPricesByHotelsAndFare: getPricesByHotelsAndFare,
}