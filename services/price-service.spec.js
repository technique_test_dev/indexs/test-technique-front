const app = require('../app');
const priceService = require('./price-service');
const hotels = app.findHotelsNearby(48.856564, 2.351711, 2000)

describe('PriceService', () => {
  
  test('getPrices() returns all prices', () => {
    expect(priceService.getPrices()).toBeDefined();
    expect(priceService.getPrices().length).toBe(248);
  })

  test('getPricesByHotelsAndFare() returns empty array', () => {
    expect(priceService.getPricesByHotelsAndFare().length).toBe(0);
  })
  
  test('getPricesByHotelsAndFare() returns empty array', () => {
    expect(priceService.getPricesByHotelsAndFare(hotels).length).toBe(0);
  })
  
  test('getPricesByHotelsAndFare() returns empty array', () => {
    expect(priceService.getPricesByHotelsAndFare(null, '11/01/2021').length).toBe(0);
  })
  
  test('getPricesByHotelsAndFare() returns empty array', () => {
    expect(priceService.getPricesByHotelsAndFare([], '11/01/2021').length).toBe(0);
  })
  
  test('getPricesByHotelsAndFare() returns all prices where hotels distance less or equal to 2000 and offer match to date and fare STANDARD', () => {
    expect(priceService.getPricesByHotelsAndFare(hotels, '11/01/2021').length).toBe(19);
  })
  
});